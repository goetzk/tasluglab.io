# TasLUG Website

## Quickstart
Clone the repository to a local directory and change into that directory:

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    $ pelican -lr

Then browse to http://127.0.0.1:8000/


## Introduction
This is the source repository for the Tasmanian Linux Users Group website.
Commits pushed to this repository will trigger a rebuild of the website using
the [Pelican](https://getpelican.com) static site generator.


## Usage
The following sections briefly outline how to setup a local environment that
can be used to test changes made to the website before being committed and
pushed to the repository.

It is assumed that the user will have [Git](https://git-scm.com/) and a recent
version of [Python 3](https://www.python.org/downloads/) installed on their
system.

For more details information on using Pelican, please refer to the
comprehensive [Pelican documentation](http://docs.getpelican.com/en/stable/).

### Setup the virtualenv
A [virtualenv](https://virtualenv.pypa.io/en/latest/) is a lightweight "virtual
environment" for Python that is used to isolate the installed Python packages
and dependencies from the system's global Python packages.

To setup the virtualenv, activate it, and install the required Python packages,
run the following commands in the directory containing the local clone of the
TasLUG website repository:

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

The virtualenv can be deactivated at any time by running:

    $ deactivate

However, the reset of these sections assume the virtualenv is active.

### Generate site files
If everything has gone to plan you should now be able to build a local copy of
the TasLUG website:

    $ pelican

or alternatively if you have `make` installed:

    $ make html

The output of the statically generate website should now be available in the
`output/` directory.

### View website
To view the locally generated of the TasLUG website, run a local HTTP server
using Python:

    $ pelican -l

or alternatively if you have `make` installed:

    $ make serve

Then browse to http://127.0.0.1:8000/

### Watch for changes
It is possible to have Pelican watch for changes to any files and automatically
regenerate the website:

    $ pelican -r

or alternatively if you have `make` installed:

    $ make regenerate

This works particularly well when combined with the `-l`/`--listen` option
which run a local a local HTTP server and regenerate the files being served up
when changes are made:

    $ pelican -lr

the `make` equivalent is:

    $ make devserver

Browse to http://127.0.0.1:8000/ and reload the page any time new changes are
make and the pages regenerated.


## Adding a post
To make a new post create a new file in the `content/` directory with a
filename using the format `YYYY-MM-DD-<post title slug>`:

    vi content/2018-11-20-new-post.md

The new file should contain some metadata at the top of the file followed by
the contents of the post using the Markdown markup language:

    Title: New Post
    Date: 2018-11-20
    Category: Hobart

    This is the content on the new post. You can use markdown to markup the
    contents and make it more visually appealing.

The new post can then be previewed by regenerating the site locally (see:
[Usage](#usage)).

Once everything looks correct, add the new file to the repository, commit the
change, and push the commit to the repository hosted on GitLab:

    $ git add content/2018-11-20-new-post.md
    $ git commit
    $ git push origin master

The site will then be automatically regenerated and deployed using the [GitLab
CI/Pages](https://docs.gitlab.com/ee/user/project/pages/) feature. This process
can take a few minutes to complete and progress can be tracked by looking at
the [Pipelines](https://gitlab.com/taslug/taslug.gitlab.io/pipelines) page.

### Pull requests
Pull requests are welcome from users who don't have write access to the TasLUG
website repository.

### Also see:
  - [Pelican documentation](http://docs.getpelican.com/en/stable/content.html)
    - detailed information about writing content for Pelican based sites.
  - [Python-Markdown documentation](https://python-markdown.github.io/)
    - information about the specific Markdown implementation used by Pelican
      and the available non-standard extensions that can be used.
