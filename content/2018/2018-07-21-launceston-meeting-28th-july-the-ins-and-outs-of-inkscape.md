Title: Launceston Meeting: 28th July - The Ins and Outs of Inkscape
Date: 2018-07-21 21:30 +1000
Category: Launceston

Hi people! For this month's Launceston meeting, we will be taking a look at the
F/OSS vector illustration program Inkscape, discussing its major features and
walking through the steps of creating a piece of vector art.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 28th July<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to
see you there!
