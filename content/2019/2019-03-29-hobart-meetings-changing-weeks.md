Title: Change of Week for Hobart TasLUG Meetings
Date: 2019-03-29
Category: Hobart

After reviewing the feedback from members about the [possibility of changing
the Hobart TasLUG meeting
day]({filename}./2019-02-27-hobart-taslug-meeting-day-survey.md) it has been
decided that **meetings will now be on the second Thursday of the month
starting from April**. Thank you to everyone who filled out the survey.

We will see you all at the next Hobart meeting on Thursday 11 April.
