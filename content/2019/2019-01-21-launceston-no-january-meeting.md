Title: Launceston January meeting postponed
Date: 2019-01-21
Category: Launceston

Hi people!

For various reasons, it doesn't make sense for us to run a meeting on the 26th of January, so we'll be skipping this month's meeting.

Apologies for the late notice, and to anybody who was hoping to catch up. Looking forward to seeing everybody next month!

~ Cheese

