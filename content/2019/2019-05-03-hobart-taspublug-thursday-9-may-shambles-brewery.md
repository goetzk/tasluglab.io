Title: Hobart TasPubLUG: Thursday 9 May - Shambles Brewery
Date: 2019-05-03
Author: John Kristensen
Category: Hobart

May is TasPubLUG time where we head to the pub to talk about Free and Open
Source software, hardware and culture... or just generally hang out and have
something to eat and drink.

Now that meetings are on the second Thursday of the month they long longer
clash with A Pint of History at the Shambles Brewery, so we figured we would
head back there.

When:
: 6:30pm, Thursday 9 May 2019

Where:
: Shambles Brewery (222 Elizabeth Street Hobart)<br/>
  [http://www.shamblesbrewery.com.au/](http://www.shamblesbrewery.com.au/)


If you are planning to come along (even for a quick drink) it would be great if
you can let [John](mailto:jkristen@theintraweb.net) know. If a largish number
of people plan to come along then I'll reserve a table.
